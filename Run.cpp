#include "Run.h"

Run::Run() : current_position(0), last_position(0)
{
}  // end default constructor

int Run::get_current_position()
{
	return current_position;
}  // end get_current_position


int Run::get_last_position()
{
   return last_position;
}  // end get_last_position


void Run::set_current_position(int pos)
{
   current_position = pos;
} // end set_current_postion

void Run::set_last_position(int pos)
{
   last_position = pos;
}  // end set_last_postion
