#ifndef GROUPPROJECT_RECORD_H
#define GROUPPROJECT_RECORD_H

#include <iostream>

using namespace std;

class Record {

private:

    string stringKey;

    string intKey;

    string concatKey;

public:

    const string &getStringKey() const;

    void setStringKey(const string &stringKey);

    const string &getIntKey() const;

    void setIntKey(const string &intKey);

    const string &getConcatKey() const;

    void setConcatKey(const string &concatKey);

    Record();

    Record(const string&, const string&);

    bool compareIntKeys(const Record& record);

    bool compareStringKeys(const Record& record);

    bool compareConcatKeys(const Record& record);

};


#endif //GROUPPROJECT_RECORD_H
