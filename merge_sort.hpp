#include "merge_sort.h"
#include <cstddef>

template<class ItemType>
merge_sort<ItemType>::merge_sort()
{
	std::list<Run> size_of_records;
	get_run_sizes();
} // end default constructor

// Gets the size of each run and puts it in a list
template<class ItemType>
void merge_sort<ItemType>::get_run_sizes()
{
	ifstream record_sizes;
    record_sizes.open("byte.txt");
    bool first_record = true;
	Run temp_run;
	int line;
	int size_of_record = 20;
	// Read in the begining and ending positions of every record. Needs a file which has the sizes of each record.
	// Each size has to be on a new line.
	while (!record_sizes.eof())
    {
        record_sizes >> line;
		if(first_record) // Handle the first record diffently than the rest.
		{
			temp_run.set_current_position(0); // The begining of the first record starts at the first (zero) position in the file
			temp_run.set_last_position(line*size_of_record - 1); // Each data (string, integer or both) in the record is length 8. Size of record multiplied by the size of each data minus one because the file position starts at zero, not one.
			size_of_records.push_back(temp_run); // Adds Run to the list of record begining and ending positions.
			first_record = false;
		}
		else
		{
			temp_run.set_current_position(temp_run.get_last_position() + 1); // Sets next record's begining position to the last record's ending position plus one.
			temp_run.set_last_position(temp_run.get_current_position() + line*size_of_record); // Sets next record's end postion to to the begining position plus the size of the record multiplied by the length of each data.
			size_of_records.push_back(temp_run); // Adds Run to the list of record begining and ending positions.
		}
    }
    record_sizes.close();
}

template<class ItemType>
bool merge_sort<ItemType>::done_sorting()
{
	if(size_of_records.empty())
		return true;
	else
		return false;
}

template<class ItemType>
ItemType merge_sort<ItemType>::get_least_record()
{
	ifstream data_file;
	data_file.open("runs.txt");
	ItemType data;
	ItemType least;
	Run temp_run;
	int current_postion;
	Run *run_to_be_iterated;
	std::list<Run>::iterator run_to_be_deleted;
		
	temp_run = size_of_records.front();
	current_postion = temp_run.get_current_position(); // gets the positon where the data is to be read.
	data_file >> least;
	for (std::list<Run>::iterator iterator = size_of_records.begin(); iterator != size_of_records.end(); iterator++)
	{
		current_postion = iterator->get_current_position();
		//data = get record; // Get either the string or int. <--
		if(least > data)
		{
			least = data;
			run_to_be_deleted = iterator; // Saves the run which has the smallest data in it.
			run_to_be_iterated = iterator;

		}
	}

	if (run_to_be_iterated->get_current_position() == run_to_be_iterated->get_last_position()) // If every data in a record is written to the sorted data file, delete the run from the list.
		size_of_records.erase(run_to_be_deleted);
	else
		run_to_be_iterated->set_current_position(run_to_be_iterated->get_current_position() + 1); // Moves current_postion to point to the next data in the record.
	
	data_file.close();

	return least;
}