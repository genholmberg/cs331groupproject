//
// Created by bry on 11/11/17.
//

#include "FileInterface.h"

const int BUFFER = 10;

FileInterface::FileInterface() {
    file = nullptr;
}

FileInterface::FileInterface(const char* _path) {
    try {

        file = fopen(_path, "r");

    } catch (const exception& e){
        e.what();
    }
}

FileInterface::~FileInterface() {
    try {

        fclose(file);

    } catch (const exception& e){
        e.what();
    }
}

bool FileInterface::Read(Record & _record) const {

    char stringKey[BUFFER];

    char intKey[BUFFER];

    int bytesRead = 0;

    bytesRead = fread(intKey, 1, 10, file);

    if(bytesRead != 10)
        return false;

    _record.setIntKey(intKey);

    bytesRead = fread(stringKey, 1, 10, file);

    if (bytesRead != 10)
        return false;

    _record.setStringKey(stringKey);

}


