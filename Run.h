#ifndef _Run
#define _Run

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class Run
{
private:
   int current_position;
   int last_position;
   
public:
   Run();
   int get_current_position();
   int get_last_position();
   void set_current_position(int pos);
   void set_last_position(int pos);

   
}; // end Run

#include "Run.cpp"
#endif