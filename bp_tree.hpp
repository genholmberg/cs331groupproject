#ifndef bp_tree_hpp
#define bp_tree_hpp
#include "Record.h"
#include <iostream>
#include <fstream>

using namespace std;

template<class ItemType>
class bp_tree
{
	private:
		struct node {
		    vector<ItemType> keys;
		    vector<node*> node_ptrs;
		    vector<ItemType*> leaf_ptrs;
		    node *parent;
			int curr_size;
		};		
		node *root;
		list<ItemType> leaf_list;	
		const int MAX_SIZE = 6;

	public:
		bp_tree()
		{
			node.parent = NULL;
			root = NULL;
			node.curr_size = 0;
			for (int i = 0; i < MAX_SIZE; i++)
			{
				node.node_ptrs[i] = NULL;
				node.leaf_ptrs[i] = NULL;
			}
		}

		bool insert_into_leaf()
		{
			//insert into file
			// insert rrn into tree
		}

		bool remove_leaf(ItemType rec)
		{
			if(node.curr_size == 0)
				false;
			
			for(int i = 0; i < node.parent.curr_size; i++)
			{
				if(*node.parent.leaf_ptrs[i] == rec) //may need to be rec pos
				{
					node.parent.leaf_ptrs.erase(i);
					node.parent.key.erase(i);
					if (node.parent.leaf_ptrs.empty() or node.parent.keys.empty())
					{
						node.parent = node.parent.parent;
					}
				}
			}
			node.curr_size--;
			
			string line;
			fstream record_file("data.txt", ios::in | ios::out | ios::app); // change to actual file
			while (getline(record_file,line))
			{
				line.replace(line.find(rec),rec.length(),"");
			}
			record_file.close();
			leaf_list.remove_if(rec);
			
			return true;
		}

		void split()
		{
			//do split thingy
		}
};

#endif