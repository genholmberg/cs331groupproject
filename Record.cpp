#include "Record.h"

Record::Record(){
    stringKey = "";
    intKey = NULL;
}

Record::Record(const string& _intKey, const string& _stringKey) {
    stringKey = _stringKey;
    intKey = _intKey;
    concatKey = _intKey + _stringKey;
}

const string &Record::getStringKey() const {
    return stringKey;
}

void Record::setStringKey(const string &stringKey) {
    Record::stringKey = stringKey;
}

const string &Record::getIntKey() const {
    return intKey;
}

void Record::setIntKey(const string &intKey) {
    Record::intKey = intKey;
}

const string &Record::getConcatKey() const {
    return concatKey;
}

void Record::setConcatKey(const string &concatKey) {
    Record::concatKey = concatKey;
}

bool Record::compareIntKeys(const Record &record) {
    return intKey < record.getIntKey();
}

bool Record::compareStringKeys(const Record &record) {
    return stringKey < record.getStringKey();
}

bool Record::compareConcatKeys(const Record &record) {
    return concatKey < record.getConcatKey()
}


